export ANDROID_HOME=/Users/Treschev/Library/Android/sdk
export DYLD_LIBRARY_PATH=/Users/Treschev/Projects/Bash/libmobiledevice/:$DYLD_LIBRARY_PATH

export PATH=${PATH}:$ANDROID_HOME/tools:$ANDROID_HOME/platform-tools
export PATH=${PATH}:/usr/local/bin
export PATH=${PATH}:/Users/Treschev/Projects/Bash/
export PATH="/opt/local/bin:/opt/local/sbin:$PATH"

#ebook
alias ebru='adb uninstall com.ebooks.ebookreader'
alias ebrud='adb uninstall com.ebooks.ebookreader.dev'
alias ebruud='ebru && ebrud'
alias ebrld='adb shell am start com.ebooks.ebookreader.dev/com.ebooks.ebookreader.ui.DevMainActivity'
alias ebrlr='adb shell am start com.ebooks.ebookreader.dev/com.ebooks.ebookreader.ui.MainActivity'
alias adbf='adb -d forward tcp:5601 tcp:5601'

#mdplayer
alias mdue='adb -e uninstall biz.mobidev.mdplayer'
alias mdud='adb -d uninstall biz.mobidev.mdplayer'
alias mds='adb shell am start biz.mobidev.mdplayer/.LauncherActivity'
