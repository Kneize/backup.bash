#!/bin/bash
#This script should share book via FB
# Destination directory for bug report
DESTINATION_FOLDER="$HOME/reports"

######### Do not modify anything below this line #########

TIMESTAMP=`date +%Y.%m.%d-%H.%M.%S`
PACKAGE_NAME=`adb shell 'dumpsys window windows' | grep -E 'mCurrentFocus' | awk '{print $3}' | sed 's/..$//'`
FOLDER_NAME=`echo $PACKAGE_NAME | cut -f 1 -d '/'`
APP_NAME=`echo $PACKAGE_NAME | tr / _`
#SCRIPT_ROOT="`dirname \"$0\"`"

mkdir -p $DESTINATION_FOLDER
cd $DESTINATION_FOLDER

mkdir -p $TIMESTAMP-$FOLDER_NAME

#Stop ebook
adb shell am force-stop com.ebooks.ebookreader

#Start new one
adb shell am start com.ebooks.ebookreader/com.ebooks.ebookreader.SplashScreen
sleep 2
#Open a book
adb shell input tap 605 775
sleep 2
#Open a menu
adb shell input keyevent 82
sleep 2
#Open a share option (two taps to slide to it, third to open)
adb shell input tap 620 1120
sleep 1
adb shell input tap 620 1120
sleep 1
adb shell input tap 620 1120

#Choose FB
adb shell input tap 530 515

#Wait for a second
sleep 5

# Catch screenshot to device
adb shell screencap /sdcard/$TIMESTAMP-$APP_NAME-screenshot.png

# Download screenshot to computer
adb pull /sdcard/$TIMESTAMP-$APP_NAME-screenshot.png "$DESTINATION_FOLDER/$TIMESTAMP-$FOLDER_NAME/$TIMESTAMP-$APP_NAME-screenshot.png"

# Delete screenshot from device
adb shell rm /sdcard/$TIMESTAMP-$APP_NAME-screenshot.png

#Press OK
adb shell input tap 364 734
