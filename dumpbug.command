#!/bin/bash

#dumpbug for Android. Unix version.

#Created by Alexander Treschev somewhere in May 2015 <svar4eg@gmail.com>

# This script gets logs, screenshot and some additional information and packs this into archive.
# Tip: you can do this to close Terminal window after script completes successfully:
# osx - Terminal - Preferences - Profiles - When the shell exits: Close if the shell exited cleanly

# Destination directory for bug report
DESTINATION_FOLDER="$HOME/Reports"

######### Do not modify anything below this line #########

TIMESTAMP=`date +%Y.%m.%d-%H.%M.%S`
SCRIPT_ROOT="`dirname \"$0\"`"
cd $SCRIPT_ROOT
#Define system
SYSVERSION=`./adb shell 'getprop ro.build.version.release'` | awk '{print $1}'

if [ 5 == $SYSVERSION ];
then
  PACKAGE_NAME=`./adb shell 'dumpsys window windows' | grep 'mFocusedApp' | awk '{print $5}' | sed 's/$//'`
elif [ 4 == $SYSVERSION ]
  then
  PACKAGE_NAME=`./adb shell 'dumpsys window windows' | grep 'mFocusedApp' | awk '{print $4}' | sed 's/....$//'`
else
  PACKAGE_NAME=`./adb shell 'dumpsys window windows' | grep 'mFocusedApp' | awk '{print $3}' | sed 's/....$//'`
fi

FOLDER_NAME=`echo $PACKAGE_NAME | cut -f 1 -d '/'`
APP_NAME=`echo $PACKAGE_NAME | tr / _`
SCRIPT_ROOT="`dirname \"$0\"`"

mkdir -p $DESTINATION_FOLDER
cd $DESTINATION_FOLDER

mkdir -p $TIMESTAMP-$FOLDER_NAME
cd $SCRIPT_ROOT

# Read logs to file
./adb shell 'logcat -v time -d' | grep $FOLDER_NAME  > "$DESTINATION_FOLDER/$TIMESTAMP-$FOLDER_NAME/$TIMESTAMP-$FOLDER_NAME-logcat.txt"

# Clean logcat
./adb logcat -c

# Catch screenshot to device
./adb shell screencap /sdcard/$TIMESTAMP-$APP_NAME-screenshot.png

# Download screenshot to computer
./adb pull /sdcard/$TIMESTAMP-$APP_NAME-screenshot.png "$DESTINATION_FOLDER/$TIMESTAMP-$FOLDER_NAME/$TIMESTAMP-$APP_NAME-screenshot.png"

# Delete screenshot from device
./adb shell rm /sdcard/$TIMESTAMP-$APP_NAME-screenshot.png

# Create archive with data
cd "$DESTINATION_FOLDER/$TIMESTAMP-$FOLDER_NAME"
zip $TIMESTAMP-$FOLDER_NAME.zip *.txt *screenshot.png
