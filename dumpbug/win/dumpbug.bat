::dumpbug for Android. Windows version.

::Created by Alexander Treschev somewhere in May 2015 <svar4eg@gmail.com>

::This script gets logs, screenshot and some additional information and packs this into archive.

::Destination directory for bug report
SET DESTINATION_FOLDER=%USERPROFILE%\Reports

::Do not modify anything below this line

SETLOCAL ENABLEEXTENSIONS
SET TIMESTAMP=%date:~-4,4%-%date:~-7,2%-%date:~-10,2%-%time:~0,2%-%time:~3,2%-%time:~6,2%

mkdir %DESTINATION_FOLDER%
cd %DESTINATION_FOLDER%

::Define active app and system version
for /f "tokens=*" %%i in ('adb shell "dumpsys window windows" ^| findstr "mFocusedApp"') do SET TMP_STRING=%%i
for /f "delims=." %%s in ('adb shell "getprop ro.build.version.release"') do  SET SYSVERSION=%%s

::Define correct activity	
IF "%SYSVERSION%" == "5" (
	for /f "tokens=5" %%a in ("%TMP_STRING%") do SET TMP_STRING=%%a
) ELSE (
IF  "%SYSVERSION%" == "4" (
	for /f "tokens=4" %%a in ("%TMP_STRING%") do SET TMP_STRING=%%a
) ELSE (
IF "%SYSVERSION%" == "3" (
	for /f "tokens=3" %%a in ("%TMP_STRING%") do SET TMP_STRING=%%a
) ELSE (
IF "%SYSVERSION%" == "2" (
	for /f "tokens=3" %%a in ("%TMP_STRING%") do SET TMP_STRING=%%a
))))

SET TMP_STRING=%TMP_STRING:~0,-3%
for /f "delims=/" %%j in ("%TMP_STRING%") do SET PACKAGE_NAME=%%j
for /f "delims=/ tokens=2" %%l in ("%TMP_STRING%") do SET APP_NAME=%%l

mkdir %TIMESTAMP%-%PACKAGE_NAME%
cd %~dp0

::Read logs to file
adb shell "logcat  -v time -d" > %DESTINATION_FOLDER%\%TIMESTAMP%-%PACKAGE_NAME%\%TIMESTAMP%-%PACKAGE_NAME%-logcat.txt 

::Clean logcat
adb shell logcat -c

::Catch screenshot to device
adb shell screencap /sdcard/%TIMESTAMP%-%APP_NAME%-screenshot.png

::Download screenshot to computer
adb pull /sdcard/%TIMESTAMP%-%APP_NAME%-screenshot.png "%DESTINATION_FOLDER%\%TIMESTAMP%-%PACKAGE_NAME%\%TIMESTAMP%-%APP_NAME%-screenshot.png"

::Delete screenshot from device
adb shell rm /sdcard/%TIMESTAMP%-%APP_NAME%-screenshot.png

::Trying to zip
cd %DESTINATION_FOLDER%\%TIMESTAMP%-%PACKAGE_NAME%

7za a -t7z  %TIMESTAMP%-%PACKAGE_NAME%.zip    *.png  *.txt