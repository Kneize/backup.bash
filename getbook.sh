#!/bin/bash
BOOK_PATH="devbooks/$1"
if [ -z "$1" ]; then
	echo "usage: $0 book-name"
else
	mkdir -p "$BOOK_PATH"
	pushd "$BOOK_PATH"
	adb pull /sdcard/Android/data/com.ebooks.ebookreader/cache/reader .
	mv META-INF/encryption.xml META-INF/encryption-bak.xml
	zip -r ../$1.epub .
	popd
fi
