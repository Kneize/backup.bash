#!/bin/bash

# This script gets logs, screenshot and some additional information and packs this into archive.

# Tip: you can do this to close Terminal window after script completes successfully:
# osx - Terminal - Preferences - Shell - When the shell exits: Close if the shell exited cleanly

# Destination directory for bug report
DESTINATION_FOLDER="$HOME/reports"

######### Do not modify anything below this line #########

TIMESTAMP=`date +%Y-%m-%d-%H-%M-%S`
echo $TIMESTAMP
SCRIPT_ROOT="`dirname \"$0\"`"

mkdir -p $DESTINATION_FOLDER
cd $DESTINATION_FOLDER

FOLDER_NAME="`ideviceinfo | grep -E 'DeviceClass' | awk '{print $2}'`"-"`ideviceinfo | grep -E 'ProductVersion' | awk '{print $2}'`"

mkdir -p $TIMESTAMP-$FOLDER_NAME
cd  $TIMESTAMP-$FOLDER_NAME

# Catch screenshot from device to current folder
idevicescreenshot

# Read logs to file
idevicesyslog > $TIMESTAMP-logs.txt & sleep 10; kill $!

# Create archive with data
cd "$DESTINATION_FOLDER/$TIMESTAMP-$FOLDER_NAME"
zip $TIMESTAMP-$FOLDER_NAME.zip *logs.txt  screenshot*.tiff

exit
