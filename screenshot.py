
from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice


device = MonkeyRunner.waitForConnection()
result = device.takeSnapshot()
result.writeToFile('screenshot.png','png')