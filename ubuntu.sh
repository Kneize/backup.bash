#!/bin/sh
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
export LD_LIBRARY_PATH=/usr/lib:/usr/local/lib

# make sure no left-over pidfiles, etc.
#######################################
rm -fr /var/run/*
rm -fr /var/lock/*

chmod 666 /system/usr/keychars/*
rm -f /tmp/tab*
mkdir -p /home/adas/Desktop
chmod 755 /home/adas/Desktop
chown -R adas.adas /home/adas/Desktop
[ -x /usr/bin/firefox-install-profile ] && /usr/bin/firefox-install-profile
[ -x /usr/local/bin/check-citrix-certs.sh ] && /usr/local/bin/check-citrix-certs.sh
[ -x /usr/bin/migrate-webapps ] && /usr/bin/migrate-webapps

# boot scripts
##############
/etc/init.d/rc S

# lock down /var for CTS
########################
chown root.adas /var/tmp
chown root.adas /var/lock
chmod 775 /var/tmp
chmod 775 /var/lock

chmod 666 /dev/socket/dbus
chmod 666 /dev/null

# runlevel 2 scripts
####################
/etc/init.d/rc 2

cp /sdcard/*.lic /data/
chmod 666 /data/*.lic
